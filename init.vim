" Configure
syntax on
set number
set ttyfast
set cc=200
set shiftwidth=4
set noswapfile
set nobackup
set clipboard=unnamedplus
set encoding=utf-8
set tabstop=4
set softtabstop=4
set smartindent
set autoindent

" Plugins
call plug#begin()

Plug 'jiangmiao/auto-pairs'
Plug 'Lokaltog/vim-monotone'
Plug 'vim-airline/vim-airline'

call plug#end()

" Keybinds
inoremap lkj <ESC>
map <C-j> <C-W>
map <C-h> <C-W>h
map <C-l> <C-W>l

" Terminal Colors
colorscheme monotone

" Compile And Run
autocmd filetype c nnoremap <F5> :w <bar> !tcc % -Wall -O2 -o %:r && ./%:r <CR>
autocmd filetype cpp nnoremap <F6> :w <bar> !g++ % -o %:r && ./%:r <CR>

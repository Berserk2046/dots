config.load_autoconfig(False)

c.content.blocking.adblock.lists = [
        "https://easylist.to/easylist/easylist.txt",
        "https://easylist.to/easylist/easyprivacy.txt"
        ]
c.content.blocking.enabled = True
c.content.blocking.method = "both"
c.content.cache.appcache = False
c.content.canvas_reading = False
c.content.cookies.accept = "never"
c.content.cookies.store = False
c.content.default_encoding = "UTF-8"
c.content.desktop_capture = False
c.content.geolocation = False
c.content.headers.do_not_track = True
c.content.javascript.enabled = False
c.content.local_storage = False
c.content.media.audio_capture = False
c.content.media.video_capture = False
c.content.media.audio_video_capture = False
c.content.mouse_lock = False
c.content.notifications.enabled = False
c.content.persistent_storage = False
c.content.private_browsing = True
c.content.webgl = False
c.url.start_pages = "https://lx.vern.cc/"
c.url.searchengines = {
        "DEFAULT":"https://lx.vern.cc/search.php?q={}"
        }

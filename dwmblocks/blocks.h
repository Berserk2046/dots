//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/

    {"^d^ ^b#ffffff^^c#000000^   ^d^ ", " free -h | grep Mem | awk '{print $3}'",	10,	0},
    {"^d^ ^b#ffffff^^c#000000^   ^d^ ", " battery",  					60,	 0},
    {"^d^ ^b#ffffff^^c#000000^   ^d^ ", " date +%H:%M",					60,	 0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "";
static unsigned int delimLen = 0;
